const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

let frontendWorkAmount = 0;
tasks.map(obj => obj.category === 'Frontend' ? frontendWorkAmount + obj.timeSpent : null);
console.log(`Общее количество времени, затраченное на работу над задачами из категории 'Frontend': ${frontendWorkAmount}`);

let bugWorkAmount = tasks.reduce((sum, current) => current.type === 'bug' ? sum + current.timeSpent : null, 0);
console.log(`Общее количество времени, затраченное на работу над задачами типа 'bug': ${bugWorkAmount}`);

let UIamount = 0;
tasks.filter(obj => obj.title.includes('UI') ? UIamount++ : null);
console.log(`Количество задач, имеющих в названии слово "UI": ${UIamount}`);

let category = tasks.reduce((acc, el) => {
    if (!acc[el.category]) {
        acc[el.category] = 1;
    } else {
        acc[el.category]++;
    }
    return acc;
}, {});
console.log(category);

let arrayOfObjects = tasks.filter(obj => obj.timeSpent > 4);
arrayOfObjects = arrayOfObjects.map(obj => {
    return {title: obj.title, category: obj.category}
});
console.log(arrayOfObjects);