import React, {Component} from 'react';
import './App.css';
import Design from './components/Design';

class App extends Component {
    state = {
        array: [],
        currentItem: '',
        currentItemPrice: '',
        currentID: '',
        currentCategory: '',
        enterWidth: 0,
        foodWidth: 0,
        carWidth: 0
    };

    changeItemName = (event) => {
        this.setState({currentItem: event.target.value})
    };

    changePrice = (event) => {
        if (isNaN(event.target.value)) {
            alert('Please enter in figures!');
        } else {
            this.setState({currentItemPrice: parseInt(event.target.value, 10)});
        }
    };

    changeCategory = (event) => {
        this.setState({currentCategory: event.target.value});
    };

    addItem = () => {
        if (this.state.currentItem === '' || this.state.currentItemPrice === '' || this.state.currentCategory === '') {
            alert('Fill in all the fields correctly!');
        } else {
            let array = [...this.state.array];
            let obj = {name: this.state.currentItem, price: this.state.currentItemPrice, category: this.state.currentCategory, id: Date.now()};
            array.unshift(obj);
            let enterWidth = (array.filter(obj => obj.category === 'Entertainment').reduce((sum, current) => sum + current.price, 0) * 100) / array.reduce((sum, current) => sum + current.price, 0);
            let foodWidth = (array.filter(obj => obj.category === 'Food').reduce((sum, current) => sum + current.price, 0) * 100) / array.reduce((sum, current) => sum + current.price, 0);
            let carWidth = (array.filter(obj => obj.category === 'Car').reduce((sum, current) => sum + current.price, 0) * 100) / array.reduce((sum, current) => sum + current.price, 0);
            this.setState({array, currentItem: '', currentItemPrice: '', enterWidth, foodWidth, carWidth});
        }
    };

    removeItem = (id) => {
        const index = this.state.array.findIndex(obj => obj.id === id);
        const array = [...this.state.array];
        array.splice(index, 1);
        this.setState({array});
    };

    render() {
        return (
            <div className="App">
                <Design remove={this.removeItem}
                        changeItemName={this.changeItemName}
                        changePrice={this.changePrice}
                        changeCategory={this.changeCategory}
                        adding={this.addItem}
                        state={this.state}/>
            </div>
        );
    }
}

export default App;
