import React from 'react';
import Items from './Items/Items';

const Design = props => {
    return (
        <div className="container">
            <div className="form">
                <input onChange={props.changeItemName} value={props.state.currentItem} className="itemName" placeholder="Item name" type="text"/>
                <input onChange={props.changePrice} value={props.state.currentItemPrice} className="itemCost" placeholder="Cost" type="text"/>
                <span>KGS</span>
                <select defaultValue="A" onChange={props.changeCategory} id="selector">
                    <option value="A" disabled>Выберите категорию</option>
                    <option value="Entertainment">Entertainment</option>
                    <option value="Car">Car</option>
                    <option value="Food">Food</option>
                </select>
                <button onClick={props.adding} id="add">Add</button>
            </div>
            <div className="list">
                <Items removeItem={props.remove} state={props.state} />
                <p>Total spent: {props.state.array.reduce((sum, current) => sum + current.price, 0)} KGS</p>
            </div>
            <div className="scale">
                <div style={{width: `${props.state.enterWidth}%`}} className="box entertainment"/>
                <div style={{width: `${props.state.foodWidth}%`}} className="box food"/>
                <div style={{width: `${props.state.carWidth}%`}} className="box car"/>
            </div>
            <div className="scaleInfo">
                <span className="red">Entertainment</span>
                <span className="green">Food</span>
                <span className="blue">Car</span>
            </div>
        </div>
    )
};

export default Design;