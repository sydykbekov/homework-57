import React from 'react';

const Items = props => {
    return props.state.array.map((item, i) => <div className="item" key={i}>{item.name} <span>({item.category})</span><span className="cost">{item.price} KGS</span><i onClick={() => props.removeItem(item.id)} className="fa fa-xing-square" aria-hidden="true"/></div>);
};

export default Items;